return {
	build = true,
	clean = true,
	compiler = {
		modules = {
			correlate = true,
			useBitLib = true,
		},
	},
}
