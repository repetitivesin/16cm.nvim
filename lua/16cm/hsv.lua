




 local H = {}
 local fmt = string.format

 H.round = function(num)

 return math.floor((num + 0.5)) end

 H["hex?"] = function(str)

 return (("string" == type(str)) and string.match(str, "^#%x%x%x%x%x%x$")) end


 H["hsv?"] = function(_1_) local _arg_2_ = _1_ local h = _arg_2_["h"] local s = _arg_2_["s"] local v = _arg_2_["v"]




 return ((function(_3_,_4_,_5_,_6_) return (_3_ == _4_) and (_4_ == _5_) and (_5_ == _6_) end)("number",type(h),type(s),type(v)) and ((0 <= h) and (h <= 360)) and ((0 <= s) and (s <= 1)) and ((0 <= v) and (v <= 1))) end



 H["hsl?"] = function(_7_) local _arg_8_ = _7_ local h = _arg_8_["h"] local s = _arg_8_["s"] local l = _arg_8_["l"]




 return ((function(_9_,_10_,_11_,_12_) return (_9_ == _10_) and (_10_ == _11_) and (_11_ == _12_) end)("number",type(h),type(s),type(l)) and ((0 <= h) and (h <= 360)) and ((0 <= s) and (s <= 1)) and ((0 <= l) and (l <= 1))) end



 H["rgb?"] = function(_13_) local _arg_14_ = _13_ local r = _arg_14_["r"] local g = _arg_14_["g"] local b = _arg_14_["b"]




 return ((function(_15_,_16_,_17_,_18_) return (_15_ == _16_) and (_16_ == _17_) and (_17_ == _18_) end)("number",type(r),type(g),type(b)) and ((0 <= r) and (r <= 255)) and ((0 <= g) and (g <= 255)) and ((0 <= b) and (b <= 255))) end


 H["hex-to-rgb"] = function(str)

 assert(H["hex?"](str), fmt("Expected hex color, got: %s", vim.inspect(str)))
 local function _20_() local tbl_17_auto = {} local i_18_auto = #tbl_17_auto for v in string.gmatch(str, "%x%x") do
 local val_19_auto = tonumber(v, 16) if (nil ~= val_19_auto) then i_18_auto = (i_18_auto + 1) do end (tbl_17_auto)[i_18_auto] = val_19_auto else end end return tbl_17_auto end local _local_19_ = _20_() local r = _local_19_[1] local g = _local_19_[2] local b = _local_19_[3]
 return {r = r, g = g, b = b} end

 H["rgb-to-hsv"] = function(rgb)

 assert(H["rgb?"](rgb), fmt("Expected RGB table, got: %s", vim.inspect(rgb)))
 local _let_22_ = {(rgb.r / 255), (rgb.g / 255), (rgb.b / 255)} local r = _let_22_[1] local g = _let_22_[2] local b = _let_22_[3]
 local m = math.min(r, g, b)
 local M = math.max(r, g, b)
 local c = (M - m)
 local R = ((M - r) / c)
 local G = ((M - g) / c)
 local B = ((M - b) / c) local h_
 if (M == m) then h_ = 0 elseif (M == r) then

 h_ = (B - G) elseif (M == g) then
 h_ = (2 + (R - B)) elseif (M == b) then
 h_ = (4 + (G - R)) else h_ = nil end local s
 if (c == 0) then s = 0 elseif true then local _ = c

 s = (c / M) else s = nil end
 local h = H.round((((h_ / 6) % 1) * 360))
 local v = M
 return {h = h, s = s, v = v} end

 H["hsl-to-hsv"] = function(hsl)

 assert(H["hsl?"](hsl), fmt("Expected HSL table, got: %s", vim.inspect(hsl)))
 local v = (hsl.l + (hsl.s * math.min(hsl.l, (1 - hsl.l))))
 local s = (((0 == v) and 0) or (2 * (1 - (hsl.l / v))))
 return {h = hsl.h, s = s, v = v} end





 local color = {}
 color.__index = color
 color.__tostring = function(self) return self["to-hex"](self) end



 color["rotate-hue!"] = function(self, delta)

 self.h = ((self.h + delta) % 360)
 return self end

 color["rotate-hue"] = function(self, delta)

 return (function(tgt, m, ...) return tgt[m](tgt, ...) end)(color.new(self), "rotate-hue!", delta) end

 color["saturate!"] = function(self, delta)

 self.s = math.max(math.min((self.s + delta), 1), 0)


 return self end

 color.saturate = function(self, delta)

 return (function(tgt, m, ...) return tgt[m](tgt, ...) end)(color.new(self), "saturate!", delta) end

 color["lighten!"] = function(self, delta) local hsl = self["to-hsl"](self)



 hsl.l = math.max(math.min((hsl.l + delta), 1), 0)


 local new_hsv = H["hsl-to-hsv"](hsl)
 self.s = new_hsv.s
 self.v = new_hsv.v
 return self end

 color.lighten = function(self, delta)


 return (function(tgt, m, ...) return tgt[m](tgt, ...) end)(color.new(self), "lighten!", delta) end

 color["brighten!"] = function(self, delta)


 self.v = math.max(math.min((self.v + delta), 1), 0)


 return self end

 color.brighten = function(self, delta)


 return (function(tgt, m, ...) return tgt[m](tgt, ...) end)(color.new(self), "brighten!", delta) end

 color["to-hsl"] = function(_25_) local _arg_26_ = _25_ local h = _arg_26_["h"] local s = _arg_26_["s"] local v = _arg_26_["v"]

 local l = (v - ((v * s) / 2)) local s0
 if (l == 0) then s0 = 0 elseif (l == 1) then s0 = 0 elseif true then local _ = l


 s0 = ((v - l) / math.min(l, (1 - l))) else s0 = nil end
 return {h = h, s = s0, l = l} end

 color["to-hsv"] = function(_28_) local _arg_29_ = _28_ local h = _arg_29_["h"] local s = _arg_29_["s"] local v = _arg_29_["v"]

 return {h = h, s = s, v = v} end

 color["to-rgb"] = function(_30_) local _arg_31_ = _30_ local h = _arg_31_["h"] local s = _arg_31_["s"] local v = _arg_31_["v"]

 local c = (v * s)
 local x = (c * (1 - math.abs((((h / 60) % 2) - 1))))
 local m = (v - c)
 local function _39_() local function _33_() local _ = h return (h < 60) end if (true and _33_()) then local _ = h
 return {c, x, 0} else local function _34_() local _ = h return (h < 120) end if (true and _34_()) then local _ = h
 return {x, c, 0} else local function _35_() local _ = h return (h < 180) end if (true and _35_()) then local _ = h
 return {0, c, x} else local function _36_() local _ = h return (h < 240) end if (true and _36_()) then local _ = h
 return {0, x, c} else local function _37_() local _ = h return (h < 300) end if (true and _37_()) then local _ = h
 return {x, 0, c} else local function _38_() local _ = h return (h < 360) end if (true and _38_()) then local _ = h
 return {c, 0, x} else return nil end end end end end end end local _let_32_ = _39_() local R = _let_32_[1] local G = _let_32_[2] local B = _let_32_[3]
 return {r = H.round((255 * (R + m))), g = H.round((255 * (G + m))), b = H.round((255 * (B + m)))} end



 color["to-hex"] = function(self)

 local _let_40_ = self["to-rgb"](self) local r = _let_40_["r"] local g = _let_40_["g"] local b = _let_40_["b"]
 return string.format("#%02X%02X%02X", r, g, b) end

 color.new = function(col)





 local obj local function _41_() local _ = col return H["hex?"](col) end if (true and _41_()) then local _ = col
 obj = H["rgb-to-hsv"](H["hex-to-rgb"](col)) else local function _42_() local _r = col.r local _g = col.g local _b = col.b return H["rgb?"](col) end if (((_G.type(col) == "table") and true and true and true) and _42_()) then local _r = col.r local _g = col.g local _b = col.b
 obj = H["rgb-to-hsv"](col) else local function _43_() local h = col.h local s = col.s local v = col.v return H["hsv?"](col) end if (((_G.type(col) == "table") and (nil ~= col.h) and (nil ~= col.s) and (nil ~= col.v)) and _43_()) then local h = col.h local s = col.s local v = col.v
 obj = {h = h, s = s, v = v} else local function _44_() local _h = col.h local _s = col.s local _l = col.l return H["hsl?"](col) end if (((_G.type(col) == "table") and true and true and true) and _44_()) then local _h = col.h local _s = col.s local _l = col.l
 obj = H["hsl-to-hsv"](col) elseif true then local _ = col
 obj = error("Expected one of:\n\9Hex color #XXXXXX or #xxxxxx\n\9RGB table {r = r, g = g, b = b}\n\9HSV table {h = h, s = s, v = v}\n\9HSL table {h = h, s = s, l = l}") else obj = nil end end end end




 return setmetatable(obj, color) end

 return color