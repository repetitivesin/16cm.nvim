 local config = require("16cm.config")
 local _local_1_ = require("16cm.util") local nil_3f = _local_1_["nil?"] local notify = _local_1_["notify"] local is_color_table_3f = _local_1_["is-color-table?"] local theme_exists_3f = _local_1_["theme-exists?"]

 local function verify_exists(path)


 local path0 = vim.fs.normalize(path)
 local file = io.open(path0, "r")
 if nil_3f(file) then
 vim.fn.mkdir(vim.fn.fnamemodify(path0, ":p:h"), "p")
 file = io.open(path0, "w") file:write("") else end return file:close() end



 local function read()

 local path = config.raw.persistence.path
 local file = io.open(path, "r")

 if nil_3f(file) then

 notify("Persistence does not exist", vim.log.levels.ERROR)
 return nil else local contents = file:read("*a") file:close()



 return vim.fn.json_decode(contents) end end

 local function write(data)

 assert((is_color_table_3f(data) or theme_exists_3f(data)), "Expected data to be in format { theme = my_theme }, where my_theme is either colors table or existing theme name.")
 local path = config.raw.persistence.path
 verify_exists(path)
 local file = io.open(path, "w") local function close_handlers_10_auto(ok_11_auto, ...) file:close() if ok_11_auto then return ... else return error(..., 0) end end local function _5_() return file:write(vim.fn.json_encode(data)) end return close_handlers_10_auto(_G.xpcall(_5_, (package.loaded.fennel or debug).traceback)) end





 local function new_persistent_apply(func)

 local function _6_(theme)
 if func(theme) then
 return write(theme) else return nil end end return _6_ end


 return {read = read, write = write, ["new-persistent-apply"] = new_persistent_apply}