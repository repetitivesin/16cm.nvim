 local _local_1_ = require("16cm.util") local notify = _local_1_["notify"] local is_color_table_3f = _local_1_["is-color-table?"] local theme_exists_3f = _local_1_["theme-exists?"]
 local colorschemes = require("16cm.colorschemes")
 local config = require("16cm.config")
 local base = require("16cm")
 local fmt = string.format

 local namespace = vim.api.nvim_create_namespace("16cm-command")

 local function display_theme(theme)

 assert((is_color_table_3f(theme) or theme_exists_3f(theme)), "Theme is either in wrong shape or doesn't exist")
 local colors do local _2_ = type(theme) if (_2_ == "string") then
 colors = colorschemes[theme] elseif (_2_ == "table") then
 colors = theme else colors = nil end end
 local ns = vim.api.nvim_create_namespace("16cm-command")
 vim.api.nvim_set_hl_ns(ns)

 local _4_ do local tbl_17_auto = {} local i_18_auto = #tbl_17_auto for _, v in ipairs({"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"}) do local val_19_auto
 do local hl_color = colors[v]
 local hl_name = ("b" .. v)
 vim.api.nvim_set_hl(ns, hl_name, {bg = hl_color, fg = hl_color})
 val_19_auto = {"BASED", hl_name} end if (nil ~= val_19_auto) then i_18_auto = (i_18_auto + 1) do end (tbl_17_auto)[i_18_auto] = val_19_auto else end end _4_ = tbl_17_auto end vim.api.nvim_echo(_4_, true, {})

 return vim.api.nvim_set_hl_ns(0) end

 local function complete(_ArgLead, CmdLine, _CursorPos)

 local iter = string.gmatch(CmdLine, "%S+")
 iter()

 local words do local tbl_17_auto = {} local i_18_auto = #tbl_17_auto for s in iter do local val_19_auto = s if (nil ~= val_19_auto) then i_18_auto = (i_18_auto + 1) do end (tbl_17_auto)[i_18_auto] = val_19_auto else end end words = tbl_17_auto end
 if ((_G.type(words) == "table") and (words[1] == nil)) then

 return {"cycle", "set", "get"} else local function _7_() return string.match(CmdLine, "get%s+") end if (((_G.type(words) == "table") and (words[1] == "get") and (words[2] == nil)) and _7_()) then


 return vim.tbl_keys(colorschemes) else local function _8_() return string.match(CmdLine, "set%s+") end if (((_G.type(words) == "table") and (words[1] == "set") and (words[2] == nil)) and _8_()) then


 return vim.tbl_keys(colorschemes) elseif true then local _ = words

 return {} else return nil end end end end

 local function command(_10_) local _arg_11_ = _10_ local fargs = _arg_11_["fargs"]

 local fargs0 do local tbl_17_auto = {} local i_18_auto = #tbl_17_auto for s in string.gmatch(table.concat(fargs, " "), "%S+") do local val_19_auto = s if (nil ~= val_19_auto) then i_18_auto = (i_18_auto + 1) do end (tbl_17_auto)[i_18_auto] = val_19_auto else end end fargs0 = tbl_17_auto end

 if ((_G.type(fargs0) == "table") and ((fargs0)[1] == "get") and ((fargs0)[2] == nil)) then



 notify(vim.inspect(config.raw.theme), vim.log.levels.INFO)
 return display_theme(config.raw.theme) elseif ((_G.type(fargs0) == "table") and ((fargs0)[1] == "get") and (nil ~= (fargs0)[2]) and ((fargs0)[3] == nil)) then local theme = (fargs0)[2]



 return display_theme(theme) elseif ((_G.type(fargs0) == "table") and ((fargs0)[1] == "set") and (nil ~= (fargs0)[2]) and ((fargs0)[3] == nil)) then local theme = (fargs0)[2]



 return base.apply(theme) elseif ((_G.type(fargs0) == "table") and ((fargs0)[1] == "cycle") and ((fargs0)[2] == nil)) then




 base.cycle()
 return notify(fmt("chose theme %s", vim.inspect(config.raw.theme))) elseif true then local _ = fargs0

 return notify("wrong syntax for command", vim.log.levels.ERROR) else return nil end end

 vim.api.nvim_create_user_command("Cm", command, {bar = true, complete = complete, desc = "16cm.nvim", nargs = "?", bang = false})








 return {command = command, complete = complete, namespace = namespace}