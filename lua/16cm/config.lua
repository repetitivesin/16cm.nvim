
 local raw = {theme = "default-dark", persistence = {path = (vim.fn.stdpath("data") .. "/16cm/persistence.txt"), enabled = false}, transparency = nil, override = {}, integrations = {["DanilaMihailov/beacon.nvim"] = true, ["nvim-treesitter/nvim-treesitter"] = true, ["akinsho/bufferline.nvim"] = true, ["anuvyklack/hydra.nvim"] = true, default = true, ["echasnovski/mini.nvim"] = true, ["folke/which-key.nvim"] = true, ["ggandor/leap.nvim"] = true, ["ggandor/lightspeed.nvim"] = true, ["glepnir/dashboard-nvim"] = true, ["glepnir/lspsaga.nvim"] = true, ["hrsh7th/nvim-cmp"] = true, ["justinmk/vim-sneak"] = true, ["lewis6991/gitsigns.nvim"] = true, ["lukas-reineke/indent-blankline.nvim"] = true, ["neoclide/coc.nvim"] = true, ["nvim-neo-tree/neo-tree.nvim"] = true, ["nvim-telescope/telescope.nvim"] = true, ["nvim-tree/nvim-tree.lua"] = true, ["p00f/nvim-ts-rainbow"] = true, ["phaazon/hop.nvim"] = true, ["rcarriga/nvim-dap-ui"] = true, ["rcarriga/nvim-notify"] = true, ["rlane/pounce.nvim"] = true, ["romgrk/barbar.nvim"] = true, ["williamboman/mason.nvim"] = true}}


































 local function extend(config)

 return vim.tbl_deep_extend("force", raw, config) end

 local function apply(config)

 for k, v in pairs(config) do
 raw[k] = v end return nil end

 return {extend = extend, raw = raw, apply = apply}