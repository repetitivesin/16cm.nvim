 local nvim_set_hl = vim.api.nvim_set_hl
 local fmt = string.format
 local hsv = require("16cm.hsv")
 local colorschemes = require("16cm.colorschemes")
 local config = require("16cm.config")

 local function tbl_3f(x)

 return (type(x) == "table") end

 local function string_3f(x)

 return (type(x) == "string") end

 local function nil_3f(x)

 return (type(x) == "nil") end

 local function theme_exists_3f(name)

 if ("string" == type(name)) then
 return not nil_3f(colorschemes[name]) else return false end end


 local function notify(message, loglevel)

 return vim.notify(fmt("16cm: %s", message), loglevel) end

 local function is_color_table_3f(tbl)

 if ("table" == type(tbl)) then local truthy_counter = 0


 for _, v in ipairs({"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"}) do
 if (("string" == type(tbl[v])) and string.match(tbl[v], "^#%x%x%x%x%x%x$")) then

 truthy_counter = (truthy_counter + 1) else end end
 return (truthy_counter == 16) else return false end end


 local function replace_with_hex(target, colors)

 local t = vim.deepcopy(target)
 if tbl_3f(target) then
 t["fg"] = (t.fg and colors[t.fg])
 do end (t)["bg"] = (t.bg and colors[t.bg])
 do end (t)["sp"] = (t.sp and colors[t.sp]) else end
 return t end

 local function hi_many(target, colors)

 for group, hi_val in pairs(target) do
 nvim_set_hl(0, group, replace_with_hex(hi_val, colors)) end return nil end


 local function apply_colors(colors)

 local integrations = require("16cm.integrations")
 for name, tbl in pairs(integrations) do
 if config.raw.integrations[name] then
 hi_many(tbl, colors) else end end

 return hi_many(config.raw.override, colors) end

 local function apply_theme(theme)



 local function _6_() local _name = theme return theme_exists_3f(theme) end if (true and _6_()) then local _name = theme

 apply_colors(colorschemes[_name]) return true else local function _7_() local _tbl = theme return is_color_table_3f(_tbl) end if (true and _7_()) then local _tbl = theme



 apply_colors(_tbl) return true else local _ = theme



 local fallback_theme
 do local _8_ = config.raw.theme local function _9_() local name = _8_ return theme_exists_3f(name) end if ((nil ~= _8_) and _9_()) then local name = _8_
 fallback_theme = name else local function _10_() local tbl = _8_ return is_color_table_3f(tbl) end if ((nil ~= _8_) and _10_()) then local tbl = _8_
 fallback_theme = tbl else local _0 = _8_ fallback_theme = "3024" end end end

 notify(fmt("applied colorscheme is either in wrong shape(table), non-existent(string), or of wrong type.\n\9Tried to apply: %s\n\9Applied: %s", vim.inspect(theme), vim.inspect(fallback_theme)), vim.log.levels.ERROR)




 apply_theme(fallback_theme) return false end end end


 local function modify_theme(colors, mod_tbl)





 local colors0 = vim.deepcopy(colors)
 for k, v in pairs(mod_tbl) do
 assert(colors0[k], fmt("Cannot modify nonexistent property %s of colors table", tostring(k)))
 do end (colors0)[k] = tostring(v(hsv.new(colors0[k]))) end



 return colors0 end

 return {["apply-colors"] = apply_colors, ["replace-with-hex"] = replace_with_hex, ["apply-theme"] = apply_theme, ["hi-many"] = hi_many, ["is-color-table?"] = is_color_table_3f, ["nil?"] = nil_3f, notify = notify, ["string?"] = string_3f, ["tbl?"] = tbl_3f, ["theme-exists?"] = theme_exists_3f, ["modify-theme"] = modify_theme}