 local config = require("16cm.config")
 local colorschemes = require("16cm.colorschemes")
 local _local_1_ = require("16cm.util") local notify = _local_1_["notify"] local apply_theme = _local_1_["apply-theme"] local modify_theme = _local_1_["modify-theme"]
 local persist = require("16cm.persistence")

 local M = {}

 M.modify_theme = function(mod_tbl)

 local cur_theme do local _2_ = type(config.raw.theme) if (_2_ == "string") then
 cur_theme = colorschemes[config.raw.theme] elseif (_2_ == "table") then
 cur_theme = config.raw.theme else cur_theme = nil end end
 return M.apply(modify_theme(cur_theme, mod_tbl)) end


 local function base_apply(theme)

 local success_3f = apply_theme(theme)
 if success_3f then
 config.raw["theme"] = theme else end
 return success_3f end


 M.apply = base_apply

 M.cycle = function()

 local current_theme = config.raw.theme
 local theme_name, _theme_tbl = nil, nil
 do local _5_ = type(current_theme) if (_5_ == "string") then
 theme_name, _theme_tbl = next(colorschemes, current_theme) else local _ = _5_
 theme_name, _theme_tbl = next(colorschemes) end end

 if not theme_name then
 theme_name, _theme_tbl = next(colorschemes) else end
 M.apply(theme_name)
 return theme_name end

 M.setup = function(override)

 local rc = config.extend((override or {}))

 if rc.persistence.enabled then

 M.apply = persist["new-persistent-apply"](base_apply)
 local _8_ = persist.read() if (nil ~= _8_) then local theme = _8_
 rc.theme = theme elseif (_8_ == nil) then
 notify("no persistent colorscheme was read, using fallback", vim.log.levels.WARN) else end else

 M.apply = base_apply end

 config.apply(rc)
 M.apply(rc.theme)
 return require("16cm.command") end

 return M