;;;;;;;;;;;;;;;;;;;;;;
;; HELPER FUNCTIONS ;;
;;;;;;;;;;;;;;;;;;;;;;
;; helper function can duplicate such in "16cm.util", but this module is intended to be standalone

(local H {})
(local fmt string.format)

(fn H.round [num]
  {:fnl/docstring "Rounds the number mathematically"}
  (math.floor (+ num 0.5)))

(fn H.hex? [str]
  {:fnl/docstring "Checks whether given argument is hex color string e.g. #AAAAAA"}
  (and (= :string (type str)) (string.match str "^#%x%x%x%x%x%x$")))

;; fnlfmt: skip
(fn H.hsv? [{: h : s : v}]
  {:fnl/docstring "Checks whether given argument is table with fields :h, :s, :v and following conditions are satisfied:
  h: type(h) == number and 0 <= h <= 360
  s: type(s) == number and 0 <= s <= 1
  v: type(v) == number and 0 <= v <= 1"}
  (and (= :number (type h) (type s) (type v)) (<= 0 h 360) (<= 0 s 1)
       (<= 0 v 1)))

;; fnlfmt: skip
(fn H.hsl? [{: h : s : l}]
  {:fnl/docstring "Checks whether given argument is table with fields :h, :s, :l and following conditions are satisfied:
  h: type(h) == number and 0 <= h <= 360
  s: type(s) == number and 0 <= s <= 1
  l: type(l) == number and 0 <= l <= 1"}
  (and (= :number (type h) (type s) (type l))
       (<= 0 h 360) (<= 0 s 1) (<= 0 l 1)))

;; fnlfmt: skip
(fn H.rgb? [{: r : g : b}]
  {:fnl/docstring "Checks whether given argument is table with fields :r, :g, :b and following conditions are satisfied:
  r: type(r) == number and 0 <= r <= 255
  g: type(g) == number and 0 <= g <= 255
  b: type(b) == number and 0 <= b <= 255"}
  (and (= :number (type r) (type g) (type b))
       (<= 0 r 255) (<= 0 g 255) (<= 0 b 255)))

(fn H.hex-to-rgb [str]
  {:fnl/docstring "Converts hex color string to rgb table in the form {: r : g : b}"}
  (assert (H.hex? str) (fmt "Expected hex color, got: %s" (vim.inspect str)))
  (local [r g b] (icollect [v (string.gmatch str "%x%x")]
                   (tonumber v 16)))
  {: r : g : b})

(fn H.rgb-to-hsv [rgb]
  {:fnl/docstring "Converts rgb table to hsv table"}
  (assert (H.rgb? rgb) (fmt "Expected RGB table, got: %s" (vim.inspect rgb)))
  (let [[r g b] [(/ rgb.r 255) (/ rgb.g 255) (/ rgb.b 255)]
        m (math.min r g b)
        M (math.max r g b)
        c (- M m)
        R (/ (- M r) c)
        G (/ (- M g) c)
        B (/ (- M b) c)
        h- (match M
             m 0
             r (- B G)
             g (+ 2 (- R B))
             b (+ 4 (- G R)))
        s (match c
            0 0
            _ (/ c M))
        h (H.round (* (% (/ h- 6) 1) 360))
        v M]
    {: h : s : v}))

(fn H.hsl-to-hsv [hsl]
  {:fnl/docstring "Converts hsl table to hsv table"}
  (assert (H.hsl? hsl) (fmt "Expected HSL table, got: %s" (vim.inspect hsl)))
  (let [v (+ hsl.l (* hsl.s (math.min hsl.l (- 1 hsl.l))))
        s (or (and (= 0 v) 0) (* 2 (- 1 (/ hsl.l v))))]
    {:h hsl.h : s : v}))

;;;;;;;;;;;;;;;;;;;;;;;;;
;; MAIN PART OF MODULE ;;
;;;;;;;;;;;;;;;;;;;;;;;;;

(local color {})
(set color.__index color)
(fn color.__tostring [self]
  {:fnl/docstring "Converts to hex color string"}
  (self:to-hex))

(fn color.rotate-hue! [self delta]
  {:fnl/docstring "Rotates color's hue in-place(i.e. impure), final value is mod 360"}
  (set self.h (% (+ self.h delta) 360))
  self)

(fn color.rotate-hue [self delta]
  {:fnl/docstring "Rotates color's hue and returns new hsv, final value is mod 360"}
  (: (color.new self) :rotate-hue! delta))

(fn color.saturate! [self delta]
  {:fnl/docstring "Saturates color in-place(i.e. impure), final saturation can't be less than 0 or greater than 1"}
  (set self.s (-> (+ self.s delta)
                  (math.min 1)
                  (math.max 0)))
  self)

(fn color.saturate [self delta]
  {:fnl/docstring "Saturates color and returs new hsv, final saturation can't be less than 0 or greater than 1"}
  (: (color.new self) :saturate! delta))

(fn color.lighten! [self delta]
  {:fnl/docstring "Lightens color in-place(i.e. impure), final lightness can't be less than 0 or greater than 1
Lightens color as if it was HSL but then converts it back to HSV."}
  (local hsl (self:to-hsl))
  (set hsl.l (-> (+ hsl.l delta)
                 (math.min 1)
                 (math.max 0)))
  (local new-hsv (H.hsl-to-hsv hsl))
  (set self.s new-hsv.s)
  (set self.v new-hsv.v)
  self)

(fn color.lighten [self delta]
  {:fnl/docstring "Lightens color and returs new hsv, final lightness can't be less than 0 or greater than 1
Lightens color as if it was HSL but then converts it back to HSV."}
  (: (color.new self) :lighten! delta))

(fn color.brighten! [self delta]
  {:fnl/docstring "Brightens color in-place(i.e. impure), final brightness (value) can't be less than 0 or greater than 1
Increases Value in HSV"}
  (set self.v (-> (+ self.v delta)
                  (math.min 1)
                  (math.max 0)))
  self)

(fn color.brighten [self delta]
  {:fnl/docstring "Brightens color and returns new hsv, final brightness (value) can't be less than 0 or greater than 1
Increases Value in HSV"}
  (: (color.new self) :brighten! delta))

(fn color.to-hsl [{: h : s : v}]
  {:fnl/docstring "Converts HSV to HSL representation, resulting table is in the form {: h : s : l}"}
  (let [l (- v (/ (* v s) 2))
        s (match l
            0 0
            1 0
            _ (/ (- v l) (math.min l (- 1 l))))]
    {: h : s : l}))

(fn color.to-hsv [{: h : s : v}]
  {:fnl/docstring "Returns HSV table in the form {: h : s : v}"}
  {: h : s : v})

(fn color.to-rgb [{: h : s : v}]
  {:fnl/docstring "Returns RGB table in the form {: r : g : b}"}
  (let [c (* v s)
        x (* c (- 1 (math.abs (- (% (/ h 60) 2) 1))))
        m (- v c)
        [R G B] (match h
                  (where _ (< h 60)) [c x 0]
                  (where _ (< h 120)) [x c 0]
                  (where _ (< h 180)) [0 c x]
                  (where _ (< h 240)) [0 x c]
                  (where _ (< h 300)) [x 0 c]
                  (where _ (< h 360)) [c 0 x])]
    {:r (H.round (* 255 (+ R m)))
     :g (H.round (* 255 (+ G m)))
     :b (H.round (* 255 (+ B m)))}))

(fn color.to-hex [self]
  {:fnl/docstring "Returns hex color string"}
  (let [{: r : g : b} (self:to-rgb)]
    (string.format "#%02X%02X%02X" r g b)))

(fn color.new [col]
  {:fnl/docstring "Constructs new color object, can take one of:
\tHex color string \"#XXXXXX\" or \"#xxxxxx\"
\tRGB table {: r : g : b}
\tHSV table {: h : s : v}
\tHSL table {: h : s : l}"}
  (local obj (match col
               (where _ (H.hex? col)) (H.rgb-to-hsv (H.hex-to-rgb col))
               (where {:r _r :g _g :b _b} (H.rgb? col)) (H.rgb-to-hsv col)
               (where {: h : s : v} (H.hsv? col)) {: h : s : v}
               (where {:h _h :s _s :l _l} (H.hsl? col)) (H.hsl-to-hsv col)
               _ (error "Expected one of:
\tHex color #XXXXXX or #xxxxxx
\tRGB table {r = r, g = g, b = b}
\tHSV table {h = h, s = s, v = v}
\tHSL table {h = h, s = s, l = l}")))
  (setmetatable obj color))

color
