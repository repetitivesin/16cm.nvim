(local config (require :16cm.config))
(local colorschemes (require :16cm.colorschemes))
(local {: notify : apply-theme : modify-theme} (require :16cm.util))
(local persist (require :16cm.persistence))

(local M {})

(fn M.modify_theme [mod-tbl]
  {:fnl/docstring "Modify current theme. Simplified version of util.modify-theme, which modifies current theme and applies it right away."}
  (let [cur-theme (match (type config.raw.theme)
                    "string" (. colorschemes config.raw.theme)
                    "table" config.raw.theme)]
    (M.apply (modify-theme cur-theme mod-tbl))))

;; Returns true if successfully applied theme and false otherwise
(fn base-apply [theme]
  {:fnl/docstring "Applies given theme."}
  (local success? (apply-theme theme))
  (when success?
    (tset config.raw :theme theme))
  success?)

;;base-apply is not set directly to support disabling persistence in M.setup
(set M.apply base-apply)

(fn M.cycle []
  {:fnl/docstring "Switches theme to the next one i.e. cycles"}
  (local current-theme config.raw.theme)
  (var (theme-name _theme-tbl)
       (match (type current-theme)
         "string" (next colorschemes current-theme)
         _ (next colorschemes)))
  ;; Next on last element return nil
  (when (not theme-name)
    (set (theme-name _theme-tbl) (next colorschemes)))
  (M.apply theme-name)
  theme-name)

(fn M.setup [override]
  {:fnl/docstring "Setup function. Takes `override` which overrides default config values found in \"16cm.config\".raw"}
  (local rc (config.extend (or override {})))

  (if rc.persistence.enabled
      (do
        (set M.apply (persist.new-persistent-apply base-apply))
        (match (persist.read)
          theme (set rc.theme theme)
          nil (notify "no persistent colorscheme was read, using fallback"
                      vim.log.levels.WARN)))
      (set M.apply base-apply))

  (config.apply rc)
  (M.apply rc.theme)
  (require :16cm.command))

M
