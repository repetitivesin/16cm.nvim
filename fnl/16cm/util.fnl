(local nvim_set_hl vim.api.nvim_set_hl)
(local fmt string.format)
(local hsv (require :16cm.hsv))
(local colorschemes (require :16cm.colorschemes))
(local config (require :16cm.config))

(fn tbl? [x]
  {:fnl/docstring "Whether argument is a table"}
  (= (type x) :table))

(fn string? [x]
  {:fnl/docstring "Whether argument is a string"}
  (= (type x) :string))

(fn nil? [x]
  {:fnl/docstring "Whether argument is nil"}
  (= (type x) :nil))

(fn theme-exists? [name]
  {:fnl/docstring "Whether given string argument is existing theme name"}
  (if (= "string" (type name))
      (not (nil? (. colorschemes name)))
      false))

(fn notify [message loglevel]
  {:fnl/docstring "Wrapper around vim.notify, which just prepends messages with \"16cm: \""}
  (vim.notify (fmt "16cm: %s" message) loglevel))

(fn is-color-table? [tbl]
  {:fnl/docstring "Checks whether given argument is of table type, and contains 16 hex colors"}
  (if (= "table" (type tbl))
      (do
        (var truthy-counter 0)
        (each [_ v (ipairs [:0 :1 :2 :3 :4 :5 :6 :7 :8 :9 :A :B :C :D :E :F])]
          (when (and (= "string" (type (. tbl v)))
                     (string.match (. tbl v) "^#%x%x%x%x%x%x$"))
            (set truthy-counter (+ truthy-counter 1))))
        (= truthy-counter 16))
      false))

(fn replace-with-hex [target colors]
  {:fnl/docstring "Copy given target and replace base16 colors with actual hex colors"}
  (local t (vim.deepcopy target))
  (when (tbl? target)
    (tset t :fg (and t.fg (. colors t.fg)))
    (tset t :bg (and t.bg (. colors t.bg)))
    (tset t :sp (and t.sp (. colors t.sp))))
  t)

(fn hi-many [target colors]
  {:fnl/docstring "Iterate over `target`(table of pairs name - highlight) and apply each highlight"}
  (each [group hi-val (pairs target)]
    (->> (replace-with-hex hi-val colors)
         (nvim_set_hl 0 group))))

(fn apply-colors [colors]
  {:fnl/docstring "Apply the colors"}
  (local integrations (require :16cm.integrations))
  (each [name tbl (pairs integrations)]
    (when (. config.raw.integrations name)
      (hi-many tbl colors)))
  ;; Apply overriding highlightings
  (hi-many config.raw.override colors))

(fn apply-theme [theme]
  {:fnl/docstring "Apply given `theme` – either valid theme name or colors table
Applies fallback theme and throws an error if given theme does not exist or is
wrongly-shaped colors table"}
  (match theme
    (where _name (theme-exists? theme)) ;; Verify such colorscheme exists
    (do (apply-colors (. colorschemes _name))
      true)

    (where _tbl (is-color-table? _tbl)) ;; Verify table has right shape
    (do (apply-colors _tbl)
      true)

    _ (do
        (local fallback-theme
               (match config.raw.theme
                 (where name (theme-exists? name)) name
                 (where tbl (is-color-table? tbl)) tbl
                 _ :3024))
        (notify
          (fmt "applied colorscheme is either in wrong shape(table), non-existent(string), or of wrong type.\n\tTried to apply: %s\n\tApplied: %s"
               (vim.inspect theme)
               (vim.inspect fallback-theme))
          vim.log.levels.ERROR)
        (apply-theme fallback-theme)
        false)))

(fn modify-theme [colors mod-tbl]
  {:fnl/docstring "Modify the theme by applying callbacks specified in mod-tbl `mod-tbl` is table in the form of pairs color-name – callback (fn [hsv] hsv)
`mod-tbl` can have arbitrary amount of keys, but all keys in `mod-tbl` must exist in `colors`
callbacks from `mod-tbl` must return value of the same type as argument e.g. HSV value
Example: (modify-theme {:0 :#AAAAAA} {:0 #($:lighten 0.2)})
Returns {:0 \"#DDDDDD\"}"}
  (local colors (vim.deepcopy colors))
  (each [k v (pairs mod-tbl)]
    (assert (. colors k) (fmt "Cannot modify nonexistent property %s of colors table" (tostring k)))
    (tset colors k (-> (. colors k)
                       (hsv.new)
                       (v)
                       (tostring))))
  colors)

{: apply-colors
 : replace-with-hex
 : apply-theme
 : hi-many
 : is-color-table?
 : nil?
 : notify
 : string?
 : tbl?
 : theme-exists?
 : modify-theme}

