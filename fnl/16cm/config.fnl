(var raw
  {:theme :default-dark
    :persistence
    {:enabled false
     :path (.. (vim.fn.stdpath "data") "/16cm/persistence.txt")}
    :transparency nil
    :override {}

    :integrations
    {:DanilaMihailov/beacon.nvim true
     :nvim-treesitter/nvim-treesitter true
     :akinsho/bufferline.nvim true
     :anuvyklack/hydra.nvim true
     :default true
     :echasnovski/mini.nvim true
     :folke/which-key.nvim true
     :ggandor/leap.nvim true
     :ggandor/lightspeed.nvim true
     :glepnir/dashboard-nvim true
     :glepnir/lspsaga.nvim true
     :hrsh7th/nvim-cmp true
     :justinmk/vim-sneak true
     :lewis6991/gitsigns.nvim true
     :lukas-reineke/indent-blankline.nvim true
     :neoclide/coc.nvim true
     :nvim-neo-tree/neo-tree.nvim true
     :nvim-telescope/telescope.nvim true
     :nvim-tree/nvim-tree.lua true
     :p00f/nvim-ts-rainbow true
     :phaazon/hop.nvim true
     :rcarriga/nvim-dap-ui true
     :rcarriga/nvim-notify true
     :rlane/pounce.nvim true
     :romgrk/barbar.nvim true
     :williamboman/mason.nvim true}})

(fn extend [config]
  {:fnl/docstring "Extend raw config table by `config` using vim.tbl_deep_extend"}
  (vim.tbl_deep_extend :force raw config))

(fn apply [config]
  {:fnl/docstring "Apply `config` to raw config table"}
  (each [k v (pairs config)]
    (tset raw k v)))

{: extend
 : raw
 : apply}
