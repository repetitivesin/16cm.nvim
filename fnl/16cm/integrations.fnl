(local M {})

(local {: nil?} (require :16cm.util))
(local {:raw {: integrations}} (require :16cm.config))

;; fnlfmt: skip
(setmetatable M
  {:__newindex
   (fn [t name highlights]
     {:fnl/docstring
      "Upon adding new integration in `M` add it to \"16cm.config\".raw.integrations.
       If :callback field is present, set __call metamethod on given integration
       table or set it to default #nil"}

     ;; Add `name` to list of integrations and enable it by default
     (when (nil? (. integrations name))
       (tset integrations name true))

     (rawset t name highlights))})

(rawset M :16cm {})

(tset M :default
      {:Base00 {:fg :0 :bg :0}
       :Base01 {:fg :1 :bg :1}
       :Base02 {:fg :2 :bg :2}
       :Base03 {:fg :3 :bg :3}
       :Base04 {:fg :4 :bg :4}
       :Base05 {:fg :5 :bg :5}
       :Base06 {:fg :6 :bg :6}
       :Base07 {:fg :7 :bg :7}
       :Base08 {:fg :8 :bg :8}
       :Base09 {:fg :9 :bg :9}
       :Base0A {:fg :A :bg :A}
       :Base0B {:fg :B :bg :B}
       :Base0C {:fg :C :bg :C}
       :Base0D {:fg :D :bg :D}
       :Base0E {:fg :E :bg :E}
       :Base0F {:fg :F :bg :F}
       :Bold {:bold true}
       :Boolean {:fg :9}
       :Character {:fg :8}
       :ColorColumn {:bg :1}
       :Comment {:fg :3}
       :Conceal {:bg :0 :fg :D}
       :Conditional {:fg :E}
       :Constant {:fg :9}
       :CurSearch {:bg :9 :fg :1}
       :Cursor {:bg :5 :fg :0}
       :CursorColumn {:bg :1}
       :CursorIM {:bg :5 :fg :0}
       :CursorLine {:bg :1}
       :CursorLineFold {:bg :1 :fg :C}
       :CursorLineNr {:bg :1 :fg :4}
       :CursorLineSign {:bg :1 :fg :3}
       :Debug {:fg :8}
       :Define {:fg :E}
       :Delimiter {:fg :F}
       :DiagnosticError {:fg :8}
       :DiagnosticFloatingError {:bg :1 :fg :8}
       :DiagnosticFloatingHint {:bg :1 :fg :D}
       :DiagnosticFloatingInfo {:bg :1 :fg :C}
       :DiagnosticFloatingWarn {:bg :1 :fg :E}
       :DiagnosticHint {:fg :D}
       :DiagnosticInfo {:fg :C}
       :DiagnosticSignError {:link :DiagnosticFloatingError}
       :DiagnosticSignHint {:link :DiagnosticFloatingHint}
       :DiagnosticSignInfo {:link :DiagnosticFloatingInfo}
       :DiagnosticSignWarn {:link :DiagnosticFloatingWarn}
       :DiagnosticUnderlineError {:sp :8 :underline true}
       :DiagnosticUnderlineHint {:sp :D :underline true}
       :DiagnosticUnderlineInfo {:sp :C :underline true}
       :DiagnosticUnderlineWarn {:sp :E :underline true}
       :DiagnosticWarn {:fg :E}
       :DiffAdd {:bg :1 :fg :B}
       :DiffAdded {:bg :0 :fg :B}
       :DiffChange {:bg :1 :fg :E}
       :DiffDelete {:bg :1 :fg :8}
       :DiffFile {:bg :0 :fg :8}
       :DiffLine {:bg :0 :fg :D}
       :DiffNewFile {:link :DiffAdded}
       :DiffRemoved {:link :DiffFile}
       :DiffText {:bg :1 :fg :D}
       :Directory {:fg :D}
       :EndOfBuffer {:fg :0}
       :Error {:bg :0 :fg :8}
       :ErrorMsg {:bg :0 :fg :8}
       :Exception {:fg :8}
       :Float {:fg :9}
       :FloatBorder {:bg :0 :fg :5}
       :FoldColumn {:bg :1 :fg :C}
       :Folded {:bg :1 :fg :3}
       :Function {:fg :D}
       :Identifier {:fg :8}
       :Ignore {:fg :C}
       :IncSearch {:bg :9 :fg :1}
       :Include {:fg :D}
       :Italic {:italic true}
       :Keyword {:fg :E}
       :Label {:fg :A}
       :LineNr {:bg :1 :fg :3}
       :LineNrAbove {:bg :1 :fg :3}
       :LineNrBelow {:bg :1 :fg :3}
       :LspCodeLens {:link :Comment}
       :LspCodeLensSeparator {:link :Comment}
       :LspReferenceRead {:link :LspReferenceText}
       :LspReferenceText {:bg :2}
       :LspReferenceWrite {:link :LspReferenceText}
       :LspSignatureActiveParameter {:link :LspReferenceText}
       :Macro {:fg :8}
       :MatchParen {:bg :2}
       :ModeMsg {:fg :B}
       :MoreMsg {:fg :B}
       :MsgArea {:bg :0 :fg :5}
       :MsgSeparator {:bg :2 :fg :4}
       :NonText {:fg :3}
       :Normal {:bg :0 :fg :5}
       :NormalFloat {:bg :1 :fg :5}
       :NormalNC {:bg :0 :fg :5}
       :Number {:fg :9}
       :Operator {:fg :5}
       :PMenu {:bg :1 :fg :5}
       :PMenuSbar {:bg :2}
       :PMenuSel {:bg :5 :fg :1}
       :PMenuThumb {:bg :7}
       :PreCondit {:fg :A}
       :PreProc {:fg :A}
       :Question {:fg :D}
       :QuickFixLine {:bg :1}
       :Repeat {:fg :A}
       :Search {:bg :A :fg :1}
       :SignColumn {:bg :1 :fg :3}
       :Special {:fg :C}
       :SpecialChar {:fg :F}
       :SpecialComment {:fg :C}
       :SpecialKey {:fg :3}
       :SpellBad {:sp :8 :undercurl true}
       :SpellCap {:sp :D :undercurl true}
       :SpellLocal {:sp :C :undercurl true}
       :SpellRare {:sp :E :undercurl true}
       :Statement {:fg :8}
       :StatusLine {:bg :2 :fg :4}
       :StatusLineNC {:bg :1 :fg :3}
       :StorageClass {:fg :A}
       :String {:fg :B}
       :Structure {:fg :E}
       :Substitute {:bg :A :fg :1}
       :TabLine {:bg :1 :fg :3}
       :TabLineFill {:bg :1 :fg :3}
       :TabLineSel {:bg :1 :fg :B}
       :Tag {:fg :A}
       :TermCursor {:reverse true}
       :TermCursorNC {:reverse true}
       :Title {:fg :D}
       :Todo {:bg :1 :fg :A}
       :TooLong {:fg :8}
       :Type {:fg :A}
       :Typedef {:fg :A}
       :Underlined {:underline true}
       :VertSplit {:bg :2 :fg :2}
       :Visual {:bg :2}
       :VisualNOS {:fg :8}
       :WarningMsg {:fg :8}
       :Whitespace {:fg :3}
       :WildMenu {:bg :A :fg :8}
       :WinBar {:bg :2 :fg :4}
       :WinBarNC {:bg :1 :fg :3}
       :WinSeparator {:bg :2 :fg :2}
       :gitcommitBranch {:bold true :fg :9}
       :gitcommitComment {:link :Comment}
       :gitcommitDiscarded {:link :Comment}
       :gitcommitDiscardedFile {:bold true :fg :8}
       :gitcommitDiscardedType {:fg :D}
       :gitcommitHeader {:fg :E}
       :gitcommitOverflow {:fg :8}
       :gitcommitSelected {:link :Comment}
       :gitcommitSelectedFile {:bold true :fg :B}
       :gitcommitSelectedType {:link :gitcommitDiscardedType}
       :gitcommitSummary {:fg :B}
       :gitcommitUnmergedFile {:link :gitcommitDiscardedFile}
       :gitcommitUnmergedType {:link :gitcommitDiscardedType}
       :gitcommitUntracked {:link :Comment}
       :gitcommitUntrackedFile {:fg :A}
       :lCursor {:bg :5 :fg :0}})

(tset M :nvim-treesitter/nvim-treesitter
      {"@comment"                     {:link :Comment}                ; line and block comments
       "@comment.todo"                {:bg :1 :fg :B}                 ; todo notes
       "@none"                        {:bg :0 :fg :5 :nocombine true} ; completely disable the highlight
       "@keyword.directive"           {:link :PreProc}                ; various preprocessor directives & shebangs
       "@keyword.directive.define"    {:link :Define}                 ; preprocessor definition directives
       "@operator"                    {:link :Operator}
       "@punctuation.delimiter"       {:link :Delimiter}
       "@punctuation.bracket"         {:link :Normal}
       "@string"                      {:link :String}                 ; string literals
       "@string.documentation"        {:link :String}                 ; string documenting code (e.g. Python docstrings)
       "@string.regexp"               {:link :SpecialChar}            ; regular expressions
       "@string.escape"               {:link :SpecialChar}            ; escape sequences
       "@string.special.symbol"       {:link :Symbol}                 ; symbols or atoms
       "@string.special.path"         {:underline true}               ; symbols or atoms
       "@string.special.uri"          {:underline true}               ; symbols or atoms
       "@markup.list"                 {:link :Special}
       "@markup.link.label"           {:link :SpecialChar}            ; other special strings (e.g. dates)
       "@markup.strong"               {:link :Bold}                   ; bold text
       "@markup.italic"               {:link :Italic}                 ; text with emphasis
       "@markup.underline"            {:link :Underlined}             ; underlined text
       "@markup.strikethrough"        {:strikethrough true}           ; strikethrough text
       "@markup.heading"              {:link :Title}                  ; text that is part of a title
       "@markup.quote"                {:link :String}                 ; text quotations
       "@markup.link.url"             {:link :Underlined}             ; URIs (e.g. hyperlinks)
       "@markup.math"                 {:link :SpecialChar}            ; math environments (e.g. `$ ... $` in LaTeX)
       "@markup.environment"          {:link :Normal}                 ; text environments of markup languages
       "@markup.environment.name"     {:bg :3 :fg :9 :bold true}      ; text indicating the type of an environment
       "@markup.link"                 {:link :Identifier}             ; text references, footnotes, citations, etc.
       "@markup.raw"                  {:link :Comment}                ; literal or verbatim text (e.g., inline code)
       "@markup.raw.block"            {:link :Comment}                ; literal or verbatim text as a stand-alone block ; (use priority 90 for blocks with injections)
       "@comment.note"                {:bg :1 :fg :D}                 ; info notes
       "@comment.warning"             {:bg :1 :fg :A}                 ; warning notes
       "@comment.error"               {:bg :1 :fg :8}                 ; danger/error notes
       "@character"                   {:link :Character}              ; character literals
       "@character.special"           {:link :SpecialCharacter}       ; special characters (e.g. wildcards)
       "@boolean"                     {:link :Boolean}                ; boolean literals
       "@number"                      {:link :Number}                 ; numeric literals
       "@number.float"                {:link :Float}                  ; floating-point number literals
       "@function"                    {:link :Function}               ; function definitions
       "@function.builtin"            {:link :Special}                ; built-in functions
       "@function.call"               {:link :Function}               ; function calls
       "@function.macro"              {:link :Macro}                  ; preprocessor macros
       "@function.method"             {:link :Function}               ; method definitions
       "@function.method.call"        {:link :Function}               ; method calls
       "@constructor"                 {:link :Special}                ; constructor calls and definitions
       "@variable.parameter"          {:link :Identifier}             ; parameters of a function
       "@keyword"                     {:link :Keyword}                ; various keywords
       "@keyword.coroutine"           {:link :Keyword}                ; keywords related to coroutines (e.g. `go` in Go, `async/await` in Python)
       "@keyword.function"            {:link :Keyword}                ; keywords that define a function (e.g. `func` in Go, `def` in Python)
       "@keyword.operator"            {:link :Keyword}                ; operators that are English words (e.g. `and` / `or`)
       "@keyword.return"              {:link :Keyword}                ; keywords like `return` and `yield`
       "@keyword.conditional"         {:link :Conditional}            ; keywords related to conditionals (e.g. `if` / `else`)
       "@keyword.conditional.ternary" {:link :Conditional}            ; ternary operator (e.g. `?` / `:`)
       "@repeat"                      {:link :Repeat}                 ; keywords related to loops (e.g. `for` / `while`)
       "@keyword.debug"               {:link :Debug}                  ; keywords related to debugging
       "@label"                       {:link :Label}                  ; GOTO and other labels (e.g. `label:` in C)
       "@keyword.include"             {:link :Include}                ; keywords for including modules (e.g. `import` / `from` in Python)
       "@keyword.exception"           {:link :Exception}              ; keywords related to exceptions (e.g. `throw` / `catch`)
       "@type"                        {:link :Type}                   ; type or class definitions and annotations
       "@type.builtin"                {:link :Type}                   ; built-in types
       "@type.definition"             {:link :Typedef}                ; type definitions (e.g. `typedef` in C)
       "@type.qualifier"              {:link :Special}                ; type qualifiers (e.g. `const`)
       "@keyword.storage"             {:link :StorageClass}           ; modifiers that affect storage in memory or life-time
       "@attribute"                   {:link :Identifier}             ; attribute annotations (e.g. Python decorators)
       "@variable.member"             {:link :Identifier}             ; object and struct fields
       "@property"                    {:link :Identifier}             ; similar to `@field`
       "@variable"                    {:link :Identifier}             ; various variable names
       "@variable.builtin"            {:link :Identifier}             ; built-in variable names (e.g. `this`)
       "@constant"                    {:link :Constant}               ; constant identifiers
       "@constant.builtin"            {:link :Special}                ; built-in constant values
       "@constant.macro"              {:link :Define}                 ; constants defined by the preprocessor
       "@module"                      {:link :Identifier}             ; modules or namespaces
       "@diff.plus"                   {:bg :B :fg :0}                 ; added text (for diff files)
       "@diff.minus"                  {:bg :8 :fg :0}                 ; deleted text (for diff files)
       "@diff.delta"                  {:bg :A :fg :0}                 ; deleted text (for diff files)
       "@tag"                         {:link :Tag}                    ; XML tag names
       "@tag.attribute"               {:link :Special}                ; XML tag attributes
       "@tag.delimiter"               {:link :Tag}})                  ; XML tag delimiters

(tset M :justinmk/vim-sneak
      {:Sneak {:bg :E :fg :0}
       :SneakLabel {:bg :E :bold true :fg :0}
       :SneakScope {:bg :7 :fg :0}})

(tset M :nvim-tree/nvim-tree.lua
      {:NvimTreeExecFile {:bold true :fg :B}
       :NvimTreeFolderIcon {:fg :3}
       :NvimTreeGitDeleted {:fg :8}
       :NvimTreeGitDirty {:fg :8}
       :NvimTreeGitMerge {:fg :C}
       :NvimTreeGitNew {:fg :A}
       :NvimTreeGitRenamed {:fg :E}
       :NvimTreeGitStaged {:fg :B}
       :NvimTreeImageFile {:bold true :fg :E}
       :NvimTreeIndentMarker {:link :NvimTreeFolderIcon}
       :NvimTreeOpenedFile {:link :NvimTreeExecFile}
       :NvimTreeRootFolder {:link :NvimTreeGitRenamed}
       :NvimTreeSpecialFile {:bold true :fg :D :underline true}
       :NvimTreeSymlink {:bold true :fg :F}
       :NvimTreeWindowPicker {:bg :1 :bold true :fg :5}})

(tset M :ggandor/leap.nvim
      {:LeapBackdrop {:link :Comment}
       :LeapLabel {:bold true :fg :8 :nocombine true}
       :LeapMatch {:bold true :fg :E :nocombine true}})

(tset M :neoclide/coc.nvim
      {:CocCodeLens {:link :LspCodeLens}
       :CocDisabled {:link :Comment}
       :CocErrorFloat {:link :DiagnosticFloatingError}
       :CocErrorHighlight {:link :DiagnosticError}
       :CocErrorSign {:link :DiagnosticSignError}
       :CocHintFloat {:link :DiagnosticFloatingHint}
       :CocHintHighlight {:link :DiagnosticHint}
       :CocHintSign {:link :DiagnosticSignHint}
       :CocInfoFloat {:link :DiagnosticFloatingInfo}
       :CocInfoHighlight {:link :DiagnosticInfo}
       :CocInfoSign {:link :DiagnosticSignInfo}
       :CocMarkdownLink {:fg :F}
       :CocMenuSel {:bg :2}
       :CocNotificationProgress {:link :CocMarkdownLink}
       :CocPumVirtualText {:link :CocMarkdownLink}
       :CocSearch {:fg :A}
       :CocSelectedText {:fg :8}
       :CocWarningFloat {:link :DiagnosticFloatingWarn}
       :CocWarningHighlight {:link :DiagnosticWarn}
       :CocWarningSign {:link :DiagnosticSignWarn}})

(tset M :phaazon/hop.nvim
      {:HopNextKey {:bold true :fg :E :nocombine true}
       :HopNextKey1 {:bold true :fg :8 :nocombine true}
       :HopNextKey2 {:bold true :fg :4 :nocombine true}
       :HopPreview {:bold true :fg :9 :nocombine true}
       :HopUnmatched {:link :Comment}})

(tset M :glepnir/lspsaga.nvim
      {:Definitions {:fg :B}
       :DefinitionsIcon {:fg :D}
       :FinderParam {:fg :8}
       :FinderSpinner {:fg :B}
       :FinderSpinnerBorder {:fg :F}
       :FinderSpinnerTitle {:link :Title}
       :FinderVirtText {:fg :9}
       :LSOutlinePreviewBorder {:fg :F}
       :LspSagaAutoPreview {:fg :F}
       :LspSagaBorderTitle {:link :Title}
       :LspSagaCodeActionBorder {:fg :F}
       :LspSagaCodeActionContent {:fg :5}
       :LspSagaCodeActionTitle {:bold true :fg :D}
       :LspSagaDefPreviewBorder {:fg :F}
       :LspSagaDiagnosticBorder {:fg :F}
       :LspSagaDiagnosticHeader {:link :Title}
       :LspSagaDiagnosticSource {:fg :E}
       :LspSagaFinderSelection {:fg :A}
       :LspSagaHoverBorder {:fg :F}
       :LspSagaLspFinderBorder {:fg :F}
       :LspSagaRenameBorder {:fg :F}
       :LspSagaSignatureHelpBorder {:fg :F}
       :OutlineDetail {:fg :3}
       :OutlineFoldPrefix {:fg :8}
       :OutlineIndentEvn {:fg :4}
       :OutlineIndentOdd {:fg :5}
       :References {:fg :B}
       :ReferencesIcon {:fg :D}
       :TargetFileName {:fg :5}})

(tset M :rlane/pounce.nvim
      {:PounceAccept {:bg :8 :bold true :fg :0 :nocombine true}
       :PounceAcceptBest {:bg :B :bold true :fg :0 :nocombine true}
       :PounceGap {:bg :3 :bold true :fg :0 :nocombine true}
       :PounceMatch {:bg :5 :bold true :fg :0 :nocombine true}})

(tset M :romgrk/barbar.nvim
      {:BufferCurrent {:bg :2 :bold true :fg :5}
       :BufferCurrentIcon {:bg :2}
       :BufferCurrentIndex {:link :BufferCurrentIcon}
       :BufferCurrentMod {:bg :2 :bold true :fg :8}
       :BufferCurrentSign {:link :BufferCurrent}
       :BufferCurrentTarget {:bg :2 :bold true :fg :E}
       :BufferInactive {:bg :1 :fg :4}
       :BufferInactiveIcon {:bg :1}
       :BufferInactiveIndex {:link :BufferInactiveIcon}
       :BufferInactiveMod {:bg :1 :fg :8}
       :BufferInactiveSign {:link :BufferInactive}
       :BufferInactiveTarget {:bg :1 :bold true :fg :E}
       :BufferOffset {:link :Normal}
       :BufferTabpageFill {:link :Normal}
       :BufferTabpages {:bg :A :bold true :fg :1}
       :BufferVisible {:bg :1 :bold true :fg :5}
       :BufferVisibleIcon {:bg :1}
       :BufferVisibleIndex {:link :BufferVisibleIcon}
       :BufferVisibleMod {:bg :1 :bold true :fg :8}
       :BufferVisibleSign {:link :BufferVisible}
       :BufferVisibleTarget {:bg :1 :bold true :fg :E}})

(tset M :lukas-reineke/indent-blankline.nvim
      {:IndentBlanklineChar {:fg :2 :nocombine true}
       :IndentBlanklineContextChar {:fg :F :nocombine true}
       :IndentBlanklineContextStart {:nocombine true :sp :F :underline true}
       :IndentBlanklineIndent1 {:fg :8 :nocombine true}
       :IndentBlanklineIndent2 {:fg :9 :nocombine true}
       :IndentBlanklineIndent3 {:fg :A :nocombine true}
       :IndentBlanklineIndent4 {:fg :B :nocombine true}
       :IndentBlanklineIndent5 {:fg :C :nocombine true}
       :IndentBlanklineIndent6 {:fg :D :nocombine true}
       :IndentBlanklineIndent7 {:fg :E :nocombine true}
       :IndentBlanklineIndent8 {:fg :F :nocombine true}})

(tset M :hrsh7th/nvim-cmp
      {:CmpItemAbbr {:fg :5}
       :CmpItemAbbrDeprecated {:fg :3}
       :CmpItemAbbrMatch {:bold true :fg :A}
       :CmpItemAbbrMatchFuzzy {:bold true :fg :A}
       :CmpItemKind {:bg :1 :fg :F}
       :CmpItemKindClass {:link :Type}
       :CmpItemKindColor {:link :Special}
       :CmpItemKindConstant {:link :Constant}
       :CmpItemKindConstructor {:link :Type}
       :CmpItemKindEnum {:link :Structure}
       :CmpItemKindEnumMember {:link :Structure}
       :CmpItemKindEvent {:link :Exception}
       :CmpItemKindField {:link :Structure}
       :CmpItemKindFile {:link :Tag}
       :CmpItemKindFolder {:link :Directory}
       :CmpItemKindFunction {:link :Function}
       :CmpItemKindInterface {:link :Structure}
       :CmpItemKindKeyword {:link :Keyword}
       :CmpItemKindMethod {:link :Function}
       :CmpItemKindModule {:link :Structure}
       :CmpItemKindOperator {:link :Operator}
       :CmpItemKindProperty {:link :Structure}
       :CmpItemKindReference {:link :Tag}
       :CmpItemKindSnippet {:link :Special}
       :CmpItemKindStruct {:link :Structure}
       :CmpItemKindText {:link :Statement}
       :CmpItemKindTypeParameter {:link :Type}
       :CmpItemKindUnit {:link :Special}
       :CmpItemKindValue {:link :Identifier}
       :CmpItemKindVariable {:link :Delimiter}
       :CmpItemMenu {:bg :1 :fg :5}})

(tset M :nvim-telescope/telescope.nvim
      {:TelescopeBorder {:fg :F}
       :TelescopeMatching {:fg :A}
       :TelescopeMultiSelection {:bg :1 :bold true}
       :TelescopeSelection {:bg :1 :bold true}})

(tset M :williamboman/mason.nvim
      {:MasonError {:fg :8}
       :MasonHeader {:bg :D :bold true :fg :0}
       :MasonHeaderSecondary {:bg :F :bold true :fg :0}
       :MasonHeading {:link :Bold}
       :MasonHighlight {:fg :F}
       :MasonHighlightBlock {:bg :F :fg :0}
       :MasonHighlightBlockBold {:link :MasonHeaderSecondary}
       :MasonHighlightBlockBoldSecondary {:link :MasonHeader}
       :MasonHighlightBlockSecondary {:bg :D :fg :0}
       :MasonHighlightSecondary {:fg :D}
       :MasonLink {:link :MasonHighlight}
       :MasonMuted {:link :Comment}
       :MasonMutedBlock {:bg :3 :fg :0}
       :MasonMutedBlockBold {:bg :3 :bold true :fg :0}})

(tset M :rcarriga/nvim-notify
      {:NotifyDEBUGBorder {:fg :3}
       :NotifyDEBUGIcon {:link :NotifyDEBUGBorder}
       :NotifyDEBUGTitle {:link :NotifyDEBUGBorder}
       :NotifyERRORBorder {:fg :8}
       :NotifyERRORIcon {:link :NotifyERRORBorder}
       :NotifyERRORTitle {:link :NotifyERRORBorder}
       :NotifyINFOBorder {:fg :C}
       :NotifyINFOIcon {:link :NotifyINFOBorder}
       :NotifyINFOTitle {:link :NotifyINFOBorder}
       :NotifyTRACEBorder {:fg :D}
       :NotifyTRACEIcon {:link :NotifyTRACEBorder}
       :NotifyTRACETitle {:link :NotifyTRACEBorder}
       :NotifyWARNBorder {:fg :E}
       :NotifyWARNIcon {:link :NotifyWARNBorder}
       :NotifyWARNTitle {:link :NotifyWARNBorder}})

(tset M :anuvyklack/hydra.nvim
      {:HydraAmaranth {:fg :E}
       :HydraBlue {:fg :D}
       :HydraHint {:link :NormalFloat}
       :HydraPink {:fg :9}
       :HydraRed {:fg :8}
       :HydraTeal {:fg :B}})

(tset M :rcarriga/nvim-dap-ui
      {:DapUIBreakpointsCurrentLine {:bold true :fg :B}
       :DapUIBreakpointsDisabledLine {:link :Comment}
       :DapUIBreakpointsInfo {:link :DiagnosticInfo}
       :DapUIBreakpointsPath {:link :Directory}
       :DapUIDecoration {:link :Title}
       :DapUIFloatBorder {:link :SpecialChar}
       :DapUILineNumber {:link :Title}
       :DapUIModifiedValue {:bold true :fg :E}
       :DapUIScope {:link :Title}
       :DapUISource {:link :Directory}
       :DapUIStoppedThread {:link :Title}
       :DapUIThread {:link :String}
       :DapUIType {:link :Type}
       :DapUIWatchesEmpty {:link :ErrorMsg}
       :DapUIWatchesError {:link :DiagnosticError}
       :DapUIWatchesValue {:link :String}})

(tset M :nvim-neo-tree/neo-tree.nvim
      {:NeoTreeDimText {:fg :3}
       :NeoTreeDotfile {:fg :4}
       :NeoTreeFadeText1 {:link :NeoTreeDimText}
       :NeoTreeFadeText2 {:fg :2}
       :NeoTreeGitAdded {:fg :B}
       :NeoTreeGitConflict {:bold true :fg :8}
       :NeoTreeGitDeleted {:fg :8}
       :NeoTreeGitModified {:fg :E}
       :NeoTreeGitUnstaged {:fg :8}
       :NeoTreeGitUntracked {:fg :A}
       :NeoTreeMessage {:bg :1 :fg :5}
       :NeoTreeModified {:fg :7}
       :NeoTreeRootName {:bold true :fg :D}
       :NeoTreeTabInactive {:fg :4}
       :NeoTreeTabSeparatorActive {:bg :2 :fg :3}
       :NeoTreeTabSeparatorInactive {:bg :1 :fg :1}})

(tset M :lewis6991/gitsigns.nvim
      {:GitSignsAdd {:bg :1 :fg :B}
       :GitSignsAddInline {:link :GitSignsAdd}
       :GitSignsAddLn {:link :GitSignsAdd}
       :GitSignsChange {:bg :1 :fg :E}
       :GitSignsChangeInline {:link :GitSignsChange}
       :GitSignsChangeLn {:link :GitSignsChange}
       :GitSignsDelete {:bg :1 :fg :8}
       :GitSignsDeleteInline {:link :GitSignsDelete}
       :GitSignsDeleteLn {:link :GitSignsDelete}})

(tset M :echasnovski/mini.nvim
      {:MiniAnimateCursor {:reverse true :nocombine true}
       :MiniAnimateNormalFloat {:link :NormalFloat}
       :MiniClueBorder {:link :NormalFloat}
       :MiniClueDescGroup {:link :DiagnosticFloatingWarn}
       :MiniClueDescSingle {:link :NormalFloat}
       :MiniClueNextKey {:link :DiagnosticFloatingHint}
       :MiniClueNextKeyWithPostkeys {:link :DiagnosticFloatingError}
       :MiniClueSeparator {:link :DiagnosticFloatingInfo}
       :MiniClueTitle {:bold true :bg :1 :fg :D}
       :MiniCompletionActiveParameter {:bg :2}
       :MiniCursorword {:underline true}
       :MiniCursorwordCurrent {:underline true}
       :MiniDepsChangeAdded {:link :diffAdded}
       :MiniDepsChangeRemoved {:link :diffRemoved}
       :MiniDepsHint {:link :DiagnosticHint}
       :MiniDepsInfo {:link :DiagnosticInfo}
       :MiniDepsMsgBreaking {:link :DiagnosticWarn}
       :MiniDepsPlaceholder {:link :Comment}
       :MiniDepsTitle {:link :Title}
       :MiniDepsTitleError {:link :DiffDelete}
       :MiniDepsTitleSame {:link :DiffText}
       :MiniDepsTitleUpdate {:link :DiffAdd}
       :MiniDiffSignAdd {:bg :1 :fg :B}
       :MiniDiffSignChange {:bg :1 :fg :E}
       :MiniDiffSignDelete {:bg :1 :fg :8}
       :MiniDiffOverAdd {:link :DiffAdd}
       :MiniDiffOverChange {:link :DiffText}
       :MiniDiffOverContext {:link :DiffChange}
       :MiniDiffOverDelete {:link :DiffDelete}
       :MiniFilesBorder {:link :NormalFloat}
       :MiniFilesBorderModified {:link :DiagnosticFloatingWarn}
       :MiniFilesCursorLine {:bg :2}
       :MiniFilesDirectory {:link :Directory}
       :MiniFilesFile {:fg :5}
       :MiniFilesNormal {:link :NormalFloat}
       :MiniFilesTitle {:bg :1 :fg :D}
       :MiniFilesTitleFocused {:bold true :bg :1 :fg :D}
       :MiniHipatternsFixme {:bold true :bg :8 :fg :0}
       :MiniHipatternsHack {:bold true :bg :E :fg :0}
       :MiniHipatternsNote {:bold true :bg :D :fg :0}
       :MiniHipatternsTodo {:bold true :bg :C :fg :0}
       :MiniIconsAzure {:fg :D}
       :MiniIconsBlue {:fg :F}
       :MiniIconsCyan {:fg :C}
       :MiniIconsGreen {:fg :B}
       :MiniIconsGrey {:fg :7}
       :MiniIconsOrange {:fg :9}
       :MiniIconsPurple {:fg :E}
       :MiniIconsRed {:fg :8}
       :MiniIconsYellow {:fg :A}
       :MiniIndentscopeSymbol {:fg :F}
       :MiniIndentscopeSymbolOff {:fg :8}
       :MiniJump {:link :SpellRare}
       :MiniJump2dDim {:link :Comment}
       :MiniJump2dSpot {:bold true :nocombine true :bg :1 :fg :7}
       :MiniJump2dSpotAhead {:nocombine true :bg :0 :fg :6}
       :MiniJump2dSpotUnique {:link :MiniJump2dSpot}
       :MiniMapNormal {:bg :1 :fg :5}
       :MiniMapSymbolCount {:fg :C}
       :MiniMapSymbolLine {:fg :D}
       :MiniMapSymbolView {:fg :F}
       :MiniNotifyBorder {:link :NormalFloat}
       :MiniNotifyNormal {:link :NormalFloat}
       :MiniNotifyTitle {:link :FloatTitle}
       :MiniOperatorsExchangeFrom {:link :IncSearch}
       :MiniPickBorder {:link :NormalFloat}
       :MiniPickBorderBusy {:bg :1 :fg :E}
       :MiniPickBorderText {:bold true :bg :1 :fg :D}
       :MiniPickCursor {:nocombine true  :blend 100}
       :MiniPickIconDirectory {:link :Directory}
       :MiniPickIconFile {:fg :5}
       :MiniPickHeader {:link :DiagnosticFloatingHint}
       :MiniPickMatchCurrent {:bg :2}
       :MiniPickMatchMarked {:bg :3}
       :MiniPickMatchRanges {:link :DiagnosticFloatingHint}
       :MiniPickNormal {:link :NormalFloat}
       :MiniPickPreviewLine {:bg :2}
       :MiniPickPreviewRegion {:link :IncSearch}
       :MiniPickPrompt {:bg :1 :fg :B}
       :MiniStarterCurrent {}
       :MiniStarterFooter {:fg :D}
       :MiniStarterHeader {:fg :D}
       :MiniStarterInactive {:link :Comment}
       :MiniStarterItem {:fg :5}
       :MiniStarterItemBullet {:fg :F}
       :MiniStarterItemPrefix {:bold true  :fg :8}
       :MiniStarterSection {:fg :F}
       :MiniStarterQuery {:bold true  :fg :B}
       :MiniStatuslineDevinfo {:bg :2 :fg :4}
       :MiniStatuslineFileinfo {:link :MiniStatuslineDevinfo}
       :MiniStatuslineFilename {:bg :1 :fg :3}
       :MiniStatuslineInactive {:link :StatusLineNC}
       :MiniStatuslineModeCommand {:bold true :bg :8 :fg :0}
       :MiniStatuslineModeInsert {:bold true :bg :D :fg :0}
       :MiniStatuslineModeNormal {:bold true :bg :5 :fg :0}
       :MiniStatuslineModeOther {:bold true :bg :3 :fg :0}
       :MiniStatuslineModeReplace {:bold true :bg :E :fg :0}
       :MiniStatuslineModeVisual {:bold true :bg :B :fg :0}
       :MiniSurround {:link :IncSearch}
       :MiniTablineCurrent {:bold true :bg :3 :fg :5}
       :MiniTablineFill {}
       :MiniTablineHidden {:bg :1 :fg :4}
       :MiniTablineModifiedCurrent {:bold true :bg :5 :fg :2}
       :MiniTablineModifiedHidden {:bg :4 :fg :1}
       :MiniTablineModifiedVisible {:bold true :bg :4 :fg :2}
       :MiniTablineTabpagesection {:bold true :bg :A :fg :1}
       :MiniTablineVisible {:bold true :bg :1 :fg :5}
       :MiniTestEmphasis {:bold true}
       :MiniTestFail {:bold true  :fg :8}
       :MiniTestPass {:bold true  :fg :B}
       :MiniTrailspace {:link :Error}})

(tset M :akinsho/bufferline.nvim
      {:BufferLineBuffer {:fg :4}
       :BufferLineBufferSelected {:bold true :fg :5}
       :BufferLineBufferVisible {:fg :5}
       :BufferLineCloseButton {:link :BufferLineBackground}
       :BufferLineCloseButtonSelected {:link :BufferLineBufferSelected}
       :BufferLineCloseButtonVisible {:link :BufferLineBufferVisible}
       :BufferLineFill {:link :Normal}
       :BufferLineTab {:bg :A :fg :0}
       :BufferLineTabSelected {:bg :A :bold true :fg :0}})

(tset M :folke/which-key.nvim
      {:WhichKey {:fg :D}
       :WhichKeyDesc {:fg :5}
       :WhichKeyFloat {:bg :1 :fg :5}
       :WhichKeyGroup {:fg :E}
       :WhichKeySeparator {:bg :1 :fg :B}
       :WhichKeyValue {:fg :3}})

(tset M :ggandor/lightspeed.nvim
      {:LightspeedCursor {:link :Cursor}
       :LightspeedGreyWash {:link :Comment}
       :LightspeedLabel {:bold true :fg :E :underline true}
       :LightspeedLabelDistant {:bold true :fg :D :underline true}
       :LightspeedMaskedChar {:fg :4}
       :LightspeedOneCharMatch {:link :LightspeedShortcut}
       :LightspeedPendingOpArea {:link :IncSearch}
       :LightspeedShortcut {:bold true :fg :7}
       :LightspeedUniqueChar {:link :LightspeedUnlabeledMatch}
       :LightspeedUnlabeledMatch {:bold true :fg :5}})

(tset M :DanilaMihailov/beacon.nvim
      {:Beacon {:bg :7}})

(tset M :glepnir/dashboard-nvim
      {:DashboardCenter {:link :Delimiter}
       :DashboardFooter {:link :Title}
       :DashboardHeader {:link :Title}
       :DashboardShortCut {:link :WarningMsg}})

M
