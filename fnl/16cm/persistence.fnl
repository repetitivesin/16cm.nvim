(local config (require :16cm.config))
(local {: nil? : notify : is-color-table? : theme-exists?} (require :16cm.util))

(fn verify-exists [path]
  {:fnl/docstring "Make sure `path` exists. If it doesn't, create it.
Boldly assumes that if `path` exists, it is normal file"}
  (local path (vim.fs.normalize path))
  (var file (io.open path :r))
  (when (nil? file)
    (vim.fn.mkdir (vim.fn.fnamemodify path ":p:h") :p)
    (set file (io.open path :w))
    (file:write ""))
  (file:close))

(fn read []
  {:fnl/docstring "Read the data from persitence file. Returns nil if file does not exist"}
  (local path config.raw.persistence.path)
  (local file (io.open path :r))

  (if (nil? file)
      ;; File does not exist
      (do (notify "Persistence does not exist" vim.log.levels.ERROR)
          nil)
      ;; File exists => return contents
      (do (local contents (file:read "*a"))
         (file:close)
         (vim.fn.json_decode contents))))

(fn write [data]
  {:fnl/docstring "Writes `data` to persistent storage. Currently `data` is assumed to be theme name or colors table"}
  (assert (or (is-color-table? data) (theme-exists? data)) "Expected data to be in format { theme = my_theme }, where my_theme is either colors table or existing theme name.")
  (local path config.raw.persistence.path)
  (verify-exists path)
  (with-open [file (io.open path :w)]
    (->
      data
      (vim.fn.json_encode)
      (file:write))))

(fn new-persistent-apply [func]
  {:fnl/docstring "Is made to wrap 16cm.apply and make it write applied theme to storage"}
  (fn [theme]
    (when (func theme)
      (write theme))))


{: read
 : write
 : new-persistent-apply}
