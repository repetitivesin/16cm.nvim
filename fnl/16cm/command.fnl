(local {: notify : is-color-table? : theme-exists?} (require :16cm.util))
(local colorschemes (require :16cm.colorschemes))
(local config (require :16cm.config))
(local base (require :16cm))
(local fmt string.format)

(local namespace (vim.api.nvim_create_namespace "16cm-command"))

(fn display-theme [theme]
  {:fnl/docstring "Print given theme colors to the :messages buffer"}
  (assert (or (is-color-table? theme) (theme-exists? theme)) "Theme is either in wrong shape or doesn't exist")
  (local colors (match (type theme)
                  "string" (. colorschemes theme)
                  "table" theme))
  (local ns (vim.api.nvim_create_namespace :16cm-command))
  (vim.api.nvim_set_hl_ns ns)
  (->
    (icollect [_ v (ipairs [:0 :1 :2 :3 :4 :5 :6 :7 :8 :9 :A :B :C :D :E :F])]
      (let [hl-color (. colors v)
                     hl-name (.. :b v)]
        (vim.api.nvim_set_hl ns hl-name {:bg hl-color :fg hl-color})
        [:BASED hl-name]))
    (vim.api.nvim_echo true {}))
  (vim.api.nvim_set_hl_ns 0))

(fn complete [_ArgLead CmdLine _CursorPos]
  {:fnl/docstring "Completion source for :Cm custom user command"}
  (local iter (string.gmatch CmdLine "%S+"))
  (iter) ;; skip first word, the command name itself
  ;; (notify (fennel.view words))
  (local words (icollect [s iter] s))
  (match words
    (where [nil])
    [:cycle :set :get]

    (where ["get" nil] (string.match CmdLine "get%s+"))
    (vim.tbl_keys colorschemes)

    (where ["set" nil] (string.match CmdLine "set%s+"))
    (vim.tbl_keys colorschemes)

    _ {}))

(fn command [{: fargs}]
  {:fnl/docstring "Handler for :Cm custom user command"}
  (local fargs (icollect [s (string.gmatch (table.concat fargs " ") "%S+")] s))
  ; (notify (fennel.view fargs))
  (match fargs
    ;; Show colors of current colorscheme
    (where ["get" nil])
    (do
      (notify (vim.inspect config.raw.theme) vim.log.levels.INFO)
      (display-theme config.raw.theme))

    ;; Show colors of selected queried colorscheme
    (where ["get" theme nil])
    (display-theme theme)

    ;; Set current theme
    (where ["set" theme nil])
    (base.apply theme)

    ;; Cycle to next theme
    ["cycle" nil]
    (do
      (base.cycle)
      (notify (fmt "chose theme %s" (vim.inspect config.raw.theme))))

    _ (notify "wrong syntax for command" vim.log.levels.ERROR)))

(vim.api.nvim_create_user_command
  "Cm"
  command
  {:bang false
   :bar true
   :complete complete
   :desc "16cm.nvim"
   :nargs "?"})

{: command
 : complete
 : namespace}
