-- Extract integration tables from mini.nvim/base16 module in a dirty way

local hl = {}
local fmt = string.format
local group = {}
local cur_group = "default"
local function hi(name, args)
	group[name] = args
end

local H = {
	has_integration = function(name)
		hl[cur_group] = vim.deepcopy(group)
		group = {}
		cur_group = name
		return true
	end,
}
local p = { base00 = "0", base01 = "1", base02 = "2", base03 = "3", base04 = "4", base05 = "5", base06 = "6", base07 = "7", base08 = "8", base09 = "9", base0A = "A", base0B = "B", base0C = "C", base0D = "D", base0E = "E", base0F = "F" }

--https://github.com/echasnovski/mini.nvim/blob/main/lua/mini/base16.lua
--https://github.com/echasnovski/mini.nvim/blob/a7c375312edba618d7abc7c6129c53c512cca9d7/lua/mini/base16.lua#L466-L1057
--- INSERT SOURCE HERE ---

--------------------------

H.has_integration("")

-- Convert attr of vim format to nvim_set_hl format
for _, integration in pairs(hl) do
  for k, v in pairs(integration) do
    if not v.attr then
      goto continue
    end
    for m in string.gmatch(v.attr, "%a+") do
      v[m] = true
    end
    v.attr = nil
    ::continue::
  end
end

-- print resulting fennel code
for k, v in pairs(hl) do
  print(fmt("(tset M :%s", k))
  print(fmt("%s)", fennel.view(v)))
  print("")
end
