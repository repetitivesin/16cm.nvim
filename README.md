<!--toc:start-->

- [About](#about)
- [Features](#features)
- [Requirements](#requirements)
- [Installation](#installation)
  - [Packer + Lua](#packer-lua)
  - [Packer + Fennel](#packer-fennel)
  - [Lazy + Lua](#lazy-lua)
  - [Lazy + Fennel](#lazy-fennel)
- [Showcase](#showcase)
  - [Get – print colors](#get-print-colors)
  - [Cycle – cycle through themes](#cycle-cycle-through-themes)
  - [Set – set theme with assist from command completion](#set-set-theme-with-assist-from-command-completion)
- [Default config](#default-config)
  - [Lua](#lua)
  - [Fennel](#fennel)
- [Persistence](#persistence)
<!--toc:end-->

# About

**16cm.nvim** is a neovim plugin that adds many base16 colorschemes as well as a
way to manage them.

# Features

- Persistence.
  No need to modify configuration each time you want new theme to persist
  between sessions
- Usercommand
  - Query colors. [Showcase](#get-print-colors)
  - Easily cycle through themes [Showcase](#cycle-cycle-through-themes)
  - Set themes with available theme completion. [Showcase](#set-set-theme-with-assist-from-command-completion)
- Integrates with every plugin that [mini/base16](https://github.com/echasnovski/mini.base16#features) integrates with
- Has 183 builtin themes

# Requirements

- Neovim stable

# Installation

## Packer + Lua

``` lua
use({
    "https://gitlab.com/repetitivesin/16cm.nvim",
    config = function() require("16cm.nvim").setup({}) end
})
```

## Packer + Fennel

``` fennel
(use {1 "https://gitlab.com/repetitivesin/16cm.nvim"
      :config #((. (require :16cm) :setup) {})})
```

## Lazy + Lua

``` lua
local lazy = require("lazy")
lazy.setup({
    {
        url = "https://gitlab.com/repetitivesin/16cm.nvim",
        opts = {}
    }
})
```

## Lazy + Fennel

``` fennel
(local lazy (require :lazy))
(lazy.setup
 [{:url "https://gitlab.com/repetitivesin/16cm.nvim"
   :opts {}}])
```

# Showcase

## Get – print colors

![get](https://gitlab.com/repetitivesin/16cm.nvim/uploads/40e7cc830f3554ff45783fd559636a4b/get.webm)

## Cycle – cycle through themes

![cycle](https://gitlab.com/repetitivesin/16cm.nvim/uploads/c149e3b24f30eac3172584f0fb3f8c57/cycle.webm)

## Set – set theme with assist from command completion

![set](https://gitlab.com/repetitivesin/16cm.nvim/uploads/fc5858aac032404fbedbf8bd4c1b2dc0/set.webm)

# Default config

## Lua

``` Lua
{
    theme = "default-dark",
    persistence = { path = (vim.fn.stdpath("data") .. "/16cm/persistence.txt"), enabled = false },
    override = {},
    integrations = {
        ["DanilaMihailov/beacon.nvim"] = true,
        ["akinsho/bufferline.nvim"] = true,
        ["anuvyklack/hydra.nvim"] = true,
        default = true,
        ["echasnovski/mini.nvim"] = true,
        ["folke/which-key.nvim"] = true,
        ["ggandor/leap.nvim"] = true,
        ["ggandor/lightspeed.nvim"] = true,
        ["glepnir/dashboard-nvim"] = true,
        ["glepnir/lspsaga.nvim"] = true,
        ["hrsh7th/nvim-cmp"] = true,
        ["justinmk/vim-sneak"] = true,
        ["lewis6991/gitsigns.nvim"] = true,
        ["lukas-reineke/indent-blankline.nvim"] = true,
        ["neoclide/coc.nvim"] = true,
        ["nvim-neo-tree/neo-tree.nvim"] = true,
        ["nvim-telescope/telescope.nvim"] = true,
        ["nvim-tree/nvim-tree.lua"] = true,
        ["p00f/nvim-ts-rainbow"] = true,
        ["phaazon/hop.nvim"] = true,
        ["rcarriga/nvim-dap-ui"] = true,
        ["rcarriga/nvim-notify"] = true,
        ["rlane/pounce.nvim"] = true,
        ["romgrk/barbar.nvim"] = true,
        ["williamboman/mason.nvim"] = true,
    },
}
```

## Fennel

``` fennel
{:theme :default-dark
 :persistence {:enabled false
               :path (.. (vim.fn.stdpath :data) :/16cm/persistence.txt)}
 :override {}
 :integrations {:DanilaMihailov/beacon.nvim true
                :akinsho/bufferline.nvim true
                :anuvyklack/hydra.nvim true
                :default true
                :echasnovski/mini.nvim true
                :folke/which-key.nvim true
                :ggandor/leap.nvim true
                :ggandor/lightspeed.nvim true
                :glepnir/dashboard-nvim true
                :glepnir/lspsaga.nvim true
                :hrsh7th/nvim-cmp true
                :justinmk/vim-sneak true
                :lewis6991/gitsigns.nvim true
                :lukas-reineke/indent-blankline.nvim true
                :neoclide/coc.nvim true
                :nvim-neo-tree/neo-tree.nvim true
                :nvim-telescope/telescope.nvim true
                :nvim-tree/nvim-tree.lua true
                :p00f/nvim-ts-rainbow true
                :phaazon/hop.nvim true
                :rcarriga/nvim-dap-ui true
                :rcarriga/nvim-notify true
                :rlane/pounce.nvim true
                :romgrk/barbar.nvim true
                :williamboman/mason.nvim true}}
```

# Persistence

Persistence allows writing theme to the file instead of storing theme in config
table, so e.g. effect of `:Cm cycle` persists after reopening neovim

To enable persistence, set `persistence = { enabled = true }` in config table
passed to `require("16cm").setup`:

``` lua
require("16cm").setup({ persistence = { enabled = true } })
```

# Builtin themes

All builtin theme names can be viewed with `:lua =vim.tbl_keys(require "16cm.colorschemes")`

# Overriding

Add your overriding highlight definitions to `override` table in config, for
instance:

``` lua
require("16cm").setup({
    override = {
        -- "5" here is the base16 color i.e. base05
        MatchParen = {bold = true, underline = true, sp = "5", fg = "5"}
    }
})
```
